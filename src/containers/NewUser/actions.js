import constants from '../../constants';

export const SUBMIT_USER = 'SUBMIT_USER';
export const RECEIVE_USER = 'RECEIVE_USERS';
export const UPDATE_BAGS = 'UPDATE_BAGS';

export const submitUserAction = (user) => ({
    type: SUBMIT_USER,
    user
})

export const receiveUserCreatedAction = (data) => ({
    type: RECEIVE_USER,
    data,
})

export const updateBagsAction = (value) => ({
    type: UPDATE_BAGS,
    value,
})


export const submitUser = user => dispatch => {
    const { config: { protocol, baseUrl, api } } = constants;
    const options =  {
        method: 'POST',
        body: JSON.stringify(user),
        headers: {
            'Content-Type': "application/json"
        }
    };
    dispatch(submitUserAction(user))
    return fetch(`${protocol}://${baseUrl}${api.basePath}${api.services.user}`, options)
        .then(response => response.json())
        .then(data => dispatch(receiveUserCreatedAction(data)))
}

