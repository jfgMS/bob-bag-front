import { UPDATE_BAGS} from './actions'

const newUserState = (state = { }, action) => {
    switch (action.type) {
        case UPDATE_BAGS:
            return {
                ...state,
                bags: action.value,
            }
        default:
            return state
    }
}

export default newUserState
