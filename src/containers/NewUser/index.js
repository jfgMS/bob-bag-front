import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import RadioButton from '../../components/RadioButton'
import { submitUser, updateBagsAction } from './actions'

class NewUser extends Component {
    static propTypes = {
        bags: PropTypes.number
    }


    render() {
        return <div className="container">
            <form>
                <p>Número de maletas</p>
                <RadioButton radios={ [
                    { value: 1, active: true },
                    { value: 2, active: true },
                    { value: 3, active: true },
                    { value: 4, active: true },
                    { value: 5, active: true }
                ]}/>
                <div className="row">
                    <p>Nombre</p>
                    <div className="input-field col s12">
                        <input id="name" type="text" className="validate" />
                    </div>
                </div>
                <div className="row">
                    <p>Email</p>
                    <div className="input-field col s12">
                        <input id="email" type="email" className="validate" />
                    </div>
                </div>
                <input type="submit" value="Enviar" className="btn waves-effect waves-light deep-orange darken-1"/>
            </form>
        </div>
    }

}

const mapStateToProps = state => {
    const { newUserState } = state;

    const {
        bags
    } = newUserState;

    return {
        bags
    }
}

export default connect(mapStateToProps)(NewUser)

