import constants from '../../constants';

export const REQUEST_USERS = 'REQUEST_USERS';
export const RECEIVE_USERS = 'RECEIVE_USERS';

export const requestUsersAction = (user) => ({
    type: REQUEST_USERS,
    user
})

export const receiveUsersAction = (data) => ({
    type: RECEIVE_USERS,
    data,
})

export const fetchUsers = user => dispatch => {
    const { config: { protocol, baseUrl, api } } = constants;
    dispatch(requestUsersAction(user))
    return fetch(`${protocol}://${baseUrl}${api.basePath}${api.services.user}`)
        .then(response => response.json())
        .then(data => dispatch(receiveUsersAction(data)))
}

