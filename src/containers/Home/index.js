import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchUsers } from './actions'
import GridContainer from '../../components/GridContainer'

class Home extends Component {
    static propTypes = {
        isLoaded: PropTypes.bool.isRequired,
        users: PropTypes.array.isRequired
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(fetchUsers())
    }

    render() {
        const { isLoaded, users } = this.props;
        if(!isLoaded) {
            return <div>Cargando...</div>
        } else {
            return <GridContainer users={users}/>
        }
    }
}

const mapStateToProps = state => {
    const { homeState } = state;
    const {
        isLoaded,
        users
    } = homeState;
    return {
        isLoaded,
        users
    }
}

export default connect(mapStateToProps)(Home)

