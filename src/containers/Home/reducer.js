import { RECEIVE_USERS, REQUEST_USERS } from './actions'

const homeState = (state = { isLoaded: false, users: []}, action) => {
    switch (action.type) {
        case REQUEST_USERS:
            return {
                ...state,
                isLoaded: false,
                users: [],
            }
        case RECEIVE_USERS:
            const result = {
                ...state,
                isLoaded: true,
                users: action.data
            }
            return result;
        default:
            return state
    }
}

export default homeState
