import React from 'react';
import PropTypes from 'prop-types';
import Navigation from '../Navigation';

const Header = ({ header, colors }) => {
    return (
        <Navigation { ...header } colors={colors} />
    )};

Header.propTypes = {
    header: PropTypes.object.isRequired,
    colors: PropTypes.object.isRequired,
};

export default Header;
