import React from 'react';
import PropTypes from 'prop-types';
import UserCard from '../UserCard';

const GridContainer = ({ users }) => {
  const userList = users.map(u => <UserCard key={u.name} { ...u }/>);
  return (
    <div className="container">
      <div className="row">
          {userList}
      </div>
    </div>
  );
};

GridContainer.propTypes = {
  users: PropTypes.array.isRequired,
};

export default GridContainer;
