import React from 'react';
import PropTypes from 'prop-types';

const Navigation = ({ logo, navigation, colors }) => {
    const navigationItems = navigation.map((i, index) => <li key={index} role="presentation"><a href={i.link}>{i.title}</a></li> )
    return (
        <nav className={colors.main}>
            <div className="nav-wrapper">
                <a href={logo.link} className="brand-logo right">{logo.title}</a>
                <ul id="nav-mobile" className="left hide-on-med-and-down">
                    {navigationItems}
                </ul>
            </div>
        </nav>
    )};

Navigation.propTypes = {
    navigation: PropTypes.array.isRequired,
};

export default Navigation;
