import React from 'react';
import PropTypes from 'prop-types';

const Footer = ({ footer, colors }) => {
    return (
        <footer className={ `page-footer ${colors.main}`}>
            <div className="container">
                <div className="row">
                    <div className="col l6 s12">
                        <h5 className="white-text">{footer.title}</h5>
                    </div>
                </div>
            </div>
            <div className="footer-copyright">
                <div className="container">
                    © 2019 {footer.copyright}
                </div>
            </div>
        </footer>
    )};

Footer.propTypes = {
    footer: PropTypes.object.isRequired,
    colors: PropTypes.object.isRequired,
};

export default Footer;
