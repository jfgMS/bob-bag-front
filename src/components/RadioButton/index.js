import React from 'react';
import PropTypes from 'prop-types';

const RadioButton = ({ radios, onClick }) => {
    const radiosHtml = radios.map((r, index) => {
        const { value, active } = r;
        return <p key={index}>
            <label>
                <input name="group1" type="radio" value={value} onClick={onClick}/>
                <span>{value}</span>
            </label>
            </p>
    })
    return (
        <div style={{display: "inline-flex"}}>
            {radiosHtml}
        </div>
    )};

RadioButton.propTypes = {
    radios: PropTypes.array.isRequired,
};

export default RadioButton;
