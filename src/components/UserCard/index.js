import React from 'react';
import PropTypes from 'prop-types';
import constants from '../../constants';


const UserCard = ({ name, level, bags }) => {
    const{ colors: { levels }} = constants;
  return (
      <div className="col s12 m6 l4">
          <div className="card horizontal">
              <div className="card-image">
              </div>
              <div className="card-stacked">
                  <div className="card-content">
                      <p>Nombre: {name}</p>
                  </div>
                  <div className={`card-action ${levels[level]}`}>
                      <p>Maletas: {bags}</p>
                  </div>

              </div>
          </div>
      </div>
  );
};

UserCard.propTypes = {
    name: PropTypes.string.isRequired,
    level: PropTypes.number.isRequired,
    bags: PropTypes.number.isRequired,
};

export default UserCard;
