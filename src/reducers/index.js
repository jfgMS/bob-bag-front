import { combineReducers } from 'redux'

import homeState from '../containers/Home/reducer';
import newUserState from '../containers/NewUser/reducer';

const rootReducer = combineReducers({
  newUserState,
  homeState
});

export default rootReducer
