import header from './header';
import colors from './colors';
import footer from './footer';
import config from  './config';

export default {
    header,
    footer,
    colors,
    config
}

