import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import reducer from './reducers'

import NewUser from './containers/NewUser'
import Home from './containers/Home'

import Header from "./components/Header/index.js";
import Footer from "./components/Footer";

import constants from './constants'

const middleware = [ thunk ]
if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger())
}

const store = createStore(
  reducer,
  applyMiddleware(...middleware)
)

render(
  <Provider store={store}>
      <Header { ...constants } />
      <Router>
          <Route exact path="/" component={Home} />
          <Route exact path="/new" component={NewUser} />
      </Router>
      <Footer { ...constants }/>
  </Provider>,
  document.getElementById('root')
)
